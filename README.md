# getting started

1. fork git repository
1. git clone your fork
1. install npm packages `npm install`
1. finish tests
   - add implementations into add empty test cases
   - update test [snapshot](https://jestjs.io/docs/snapshot-testing)
   - `npm run test` need to be success
1. fix linting errors
   - `npm run lint` need to be success
1. create merge(/pull) request from your fork
   - CI need to be green!
   - **add your name into merge request title**
